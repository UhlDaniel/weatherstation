from fastapi import FastAPI
from typing import Union

app = FastAPI()


@app.get("/weatherstation/{data}")
async def read_weather_raw(data: str):
    """Function used to see the raw data sent by the weather station. Use this
    function to adjust the read_weather function in case parameters are
    missing.
    """

    return {"data": data}


@app.get("/data/report/")
async def read_weather(ID: Union[str, None] = None,
                       PASSWORD: Union[str, None] = None,
                       tempf: Union[str, None] = None,
                       humidity: Union[str, None] = None,
                       dewptf: Union[str, None] = None,
                       windchillf: Union[str, None] = None,
                       winddir: Union[str, None] = None,
                       windspeedmph: Union[str, None] = None,
                       windgustmph: Union[str, None] = None,
                       rainin: Union[str, None] = None,
                       dailyrainin: Union[str, None] = None,
                       weeklyrainin: Union[str, None] = None,
                       monthlyrainin: Union[str, None] = None,
                       yearlyrainin: Union[str, None] = None,
                       solarradiation: Union[str, None] = None,
                       indoortempf: Union[str, None] = None,
                       indoorhumidity: Union[str, None] = None,
                       absbaromin: Union[str, None] = None,
                       baromin: Union[str, None] = None,
                       lowbatt: Union[str, None] = None,
                       dateutc: Union[str, None] = None,
                       softwaretype: Union[str, None] = None,
                       action: Union[str, None] = None,
                       realtime: Union[str, None] = None,
                       rtfreq: Union[str, None] = None,
                       ):
    """Capture weathercloud data from an Ecowitt weather station.

    This function can be used to capture the climate data of a Ecowitt weather
    station. Set the server path at your Ecowitt device to '<your-IP>/data/report/'
    and adjust this function to your needs.
    """

    return {"data": ID}
